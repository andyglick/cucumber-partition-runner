# Cucumber Partition Runner

On projects with a very large number of Cucumber scenarios, there are times where you may need to partition the tests
so you can run groups on different servers in parallel. You can of course do this with tags or by organizing your
features into different directories. However both of these approaches are difficult to change if you need to add or 
remove one of the partitions. 

## Usage

Typically you run your cucumber tests using  a runner like this:

    @RunWith(Cucumber.class)
    public class RunCucumberTest extends TestCase {
    }

By using CucumberPartitioned.class in place of Cucumber.class, you can specify how many partitions into which you want
to divide the scenarios and which division you want to run.

    @RunWith(CucumberPartitioned.class)
    public class RunCucumberTest extends TestCase {
    }

To set the number of partitions, pass in a parameter as a system property of cucumber.partition.qty. To tell the runner 
which partition to execute use the system property of cucumber.partition.id.

For example:

    mvn clean verify -Dcucumber.partition.id=2 -Dcucumber.partition.qty=3 
    
If you want to see how scenarios are being assigned you can run it with

    -Dcucumber.partition.debug=true

This will show you what partition each scenario will run under with an asterisk next to the ones that will run in this
environment.

## Instalation

First you need to get it into a repository you can access. If you are using Maven simply checkout the code and then run:

    maven clean install
    
Then add the dependency to your pom. You may need to exclude cucumber-picocontainer to keep it from conflicting with
Spring. This exclusion is also shown below. 

		<dependency>
			<groupId>net.xeric.utils.test</groupId>
			<artifactId>cucumber-partition-runner</artifactId>
			<version>1.0</version>
			<exclusions>
				<exclusion>
					<groupId>info.cukes</groupId>
					<artifactId>cucumber-picocontainer</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

## How it works

Each scenario is assigned a number from 1 to cucumber.partition.qty. If this number matches cucumber.partition.id, then
it is run on that machine. So if you set qty to 3, you should make sure you have three environments set to 1, 2, and 3. 
The scenarios are assigned to one of the partitions based on a hash of the scenario's name. For most projects this should
distribute the scenario's fairly evenly between the different partitions.

