package net.xeric.utils.test.cucumber;


import cucumber.runtime.CucumberException;
import cucumber.runtime.Runtime;
import cucumber.runtime.junit.ExecutionUnitRunner;
import cucumber.runtime.junit.JUnitReporter;
import cucumber.runtime.junit.ScenarioOutlineRunner;
import cucumber.runtime.model.CucumberFeature;
import cucumber.runtime.model.CucumberScenario;
import cucumber.runtime.model.CucumberScenarioOutline;
import cucumber.runtime.model.CucumberTagStatement;
import gherkin.formatter.model.Feature;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.ParentRunner;
import org.junit.runners.model.InitializationError;

public class FeatureRunnerPartitioned extends ParentRunner<ParentRunner> {
    private final List<ParentRunner> children = new ArrayList<ParentRunner>();

    private final CucumberFeature cucumberFeature;
    private final Runtime runtime;
    private final JUnitReporter jUnitReporter;
    private Description description;
    private int cucumberPartitionQty = 0;
    private int cucumberPartitionId = 0;
    private boolean debug = false;

    public FeatureRunnerPartitioned(CucumberFeature cucumberFeature, Runtime runtime, JUnitReporter jUnitReporter) throws InitializationError {
        super(null);
        setPartitionInfo();
        this.cucumberFeature = cucumberFeature;
        this.runtime = runtime;
        this.jUnitReporter = jUnitReporter;
        buildFeatureElementRunners();
    }

    private void setPartitionInfo() {

        try {
            cucumberPartitionQty = Integer.valueOf(System.getProperty("cucumber.partition.qty"));
            cucumberPartitionId = Integer.valueOf(System.getProperty("cucumber.partition.id"));
            debug = Boolean.valueOf(System.getProperty("cucumber.partition.debug","false"));
        } catch (RuntimeException e) {
            printPartitionInstructions();
            throw new RuntimeException("CucumberPartition Not Properly Configured");
        }

        if(cucumberPartitionId < 1 ||
           cucumberPartitionQty < 1 ||
           cucumberPartitionId > cucumberPartitionQty) {
            printPartitionInstructions();
            throw new RuntimeException("CucumberPartition Not Propertly Configured");
        }


    }

    private void printPartitionInstructions() {
        System.out.println("To use CucumberPartitioned, you must pass two system variables. For example:");
        System.out.println("-Dcucumber.partition.id=2 -Dcucumber.partition.qty=5");
        System.out.println("cucumber.partition.qty is the total number of partitions");
        System.out.println("cucumber.partition.id is the id of the particular partition");
        System.out.println("There should be an environment with in id from 1 through the qty of partitions.");
        System.out.println("cucumber.partition.id and cucumber.partition.qty must be present and greater than 0");
        System.out.println("cucumber.partition.id cannot be more than cucumber.partition.qty");
    }

    @Override
    public String getName() {
        Feature feature = cucumberFeature.getGherkinFeature();
        return feature.getKeyword() + ": " + feature.getName();
    }

    @Override
    public Description getDescription() {
        if (description == null) {
            description = Description.createSuiteDescription(getName(), cucumberFeature.getGherkinFeature());
            for (ParentRunner child : getChildren()) {
                description.addChild(describeChild(child));
            }
        }
        return description;
    }

    @Override
    protected List<ParentRunner> getChildren() {
        return children;
    }

    @Override
    protected Description describeChild(ParentRunner child) {
        return child.getDescription();
    }

    @Override
    protected void runChild(ParentRunner child, RunNotifier notifier) {
        child.run(notifier);
    }

    @Override
    public void run(RunNotifier notifier) {
        jUnitReporter.uri(cucumberFeature.getPath());
        jUnitReporter.feature(cucumberFeature.getGherkinFeature());
        super.run(notifier);
        jUnitReporter.eof();
    }

    private void buildFeatureElementRunners() {



        for (CucumberTagStatement cucumberTagStatement : cucumberFeature.getFeatureElements()) {
            try {
                ParentRunner featureElementRunner;
                if (cucumberTagStatement instanceof CucumberScenario) {
                    featureElementRunner = new ExecutionUnitRunner(runtime, (CucumberScenario) cucumberTagStatement, jUnitReporter);
                } else {
                    featureElementRunner = new ScenarioOutlineRunner(runtime, (CucumberScenarioOutline) cucumberTagStatement, jUnitReporter);
                }
                int assignToPartition = Math.abs(featureElementRunner.getDescription().getDisplayName().hashCode()) % cucumberPartitionQty + 1;
                if(debug) {
                    System.out.print(cucumberPartitionId == assignToPartition ? "*" : " ");
                    System.out.print("Partition " + assignToPartition);
                    System.out.println(" assigned to " + featureElementRunner.getDescription().getDisplayName());
                }
                if(cucumberPartitionId == assignToPartition) {
                    children.add(featureElementRunner);
                }
            } catch (InitializationError e) {
                throw new CucumberException("Failed to create scenario runner", e);
            }
        }
    }
}

