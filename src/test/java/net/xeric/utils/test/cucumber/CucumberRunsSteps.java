package net.xeric.utils.test.cucumber;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertTrue;

/**
 * Created by markshead on 2/28/16.
 */
public class CucumberRunsSteps {
    @When("^maven runs$")
    public void mavenRuns() throws Throwable {
        //Nothing to do here
    }

    @Then("^the tests pass$")
    public void theTestsPass() throws Throwable {
        assertTrue(true);
    }
}
