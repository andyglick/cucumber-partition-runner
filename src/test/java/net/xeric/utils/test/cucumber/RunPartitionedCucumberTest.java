package net.xeric.utils.test.cucumber;

import cucumber.api.CucumberOptions;
import junit.framework.TestCase;
import org.junit.runner.RunWith;

/**
 * Created by markshead on 2/28/16.
 *
 * This runs the tests that are tagged with @partitioned using the partition runner
 * instead of the normal Cucumber JUnit runner.
 */
@RunWith(CucumberPartitioned.class)
@CucumberOptions(tags = {"@partitioned"})
public class RunPartitionedCucumberTest extends TestCase {
}
